-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: std-mysql    Database: std_1203_exam
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `films`
--

DROP TABLE IF EXISTS `films`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `description` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `production_year` year(4) NOT NULL,
  `country` varchar(25) NOT NULL,
  `producer` varchar(56) NOT NULL,
  `scenarist` varchar(56) NOT NULL,
  `actors` varchar(100) NOT NULL,
  `time_in_m` int(11) NOT NULL,
  `poster_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poster_id` (`poster_id`),
  KEY `type_id` (`type_id`) USING BTREE,
  CONSTRAINT `films_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `films_ibfk_2` FOREIGN KEY (`poster_id`) REFERENCES `posters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films`
--

LOCK TABLES `films` WRITE;
/*!40000 ALTER TABLE `films` DISABLE KEYS */;
INSERT INTO `films` VALUES (3,'Пальма ','1977 год. Игорь Польский улетает за границу и вынужден оставить свою овчарку по кличке Пальма прямо на взлетной полосе: по недоразумению у собаки нет медицинской справки. Пальма прячется в аэропорту и изо дня в день встречает самолеты в надежде, что хозяин вернётся. Но время идет… Овчарка становится талисманом аэропорта, оказываясь в центре всех важных событий в жизни его обитателей. А для 9-летнего Коли, который после гибели мамы приезжает к отцу, пилоту Вячеславу Лазареву, Пальма и вовсе становится лучшим другом. Лазареву предстоит заслужить доверие сына, сделав нелегкий выбор между карьерой и семьей и решив, что делать, когда за Пальмой возвращается ее настоящий хозяин.',5,2020,'Россия, Япония','Александр Домогаров мл.','Александр Домогаров мл.','Виктор Добронравов, Лилия, Леонид Басов, Владимир Ильин',111,1),(4,'Ученик экзорциста','Молодой священник в первый же день практики в школе экзорцистов сталкивается с очень сложным случаем одержимости. Под руководством бывалого экзорциста-наставника ему придется погрузиться в ад на земле, столкнуться с собственными демонами и разобраться, почему, хотя школа готовит всё новых экзорцистов, одержимость распространяется как эпидемия.',2,2020,'США','Джастин П. Ланж','Джастин П. Ланж','Гай Пирс, Стивен Лэнг, Кит Дэвид, Робин Бартлетт',87,4),(19,'Годзилла против Конга','Когда в наш мир явится новая угроза, два мифических монстра сойдутся в захватывающем поединке. Конг и группа ученых отправляются в опасное путешествие в поисках родного дома гиганта. Среди них девочка Джия, единственная, кто умеет общаться с Конгом. Неожиданно они сталкиваются с разъяренным Годзиллой, разрушающим все на своем пути. Битва двух титанов, спровоцированная неведомыми силами, - лишь малая часть тайны, спрятанной в недрах Земли.',3,2021,'США','Адам Вингард','Эрик Пирсон, Макс Боренштейн, Терри Россио','Эйса Гонсалес, Милли Бобби Браун, Александр Скарсгард, Ребекка Холл, Кайл Чандлер\r\n',114,1),(20,'Поступь хаоса ','Недалекое будущее. Тодд обнаруживает Виолу, загадочную девушку, потерпевшую крушение на его родной планете, где женщины давно исчезли, а все мужчины страдают от вируса – существуют в пространстве бесконечных мыслеобразов, которое называют Шумом: рядом с другим человеком совершенно невозможно утаить свои воспоминания и мысли. В такой агрессивной реальности Виоле, чьи чувства остаются скрытыми, не выжить без помощи Тодда. Вместе им предстоит отправиться в опасное путешествие и столкнуться с самыми темными тайнами планеты.',3,2020,'США','Рэй Анджелик, Даг Дэвисон, Эрик Фейг','Патрик Несс, Кристофер Д. Форд','ом Холланд, Мадс Миккельсен, Дэйзи Ридли, Ник Джонас',109,2);
/*!40000 ALTER TABLE `films` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posters`
--

DROP TABLE IF EXISTS `posters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(25) NOT NULL,
  `MIME` varchar(100) NOT NULL,
  `MD5_hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posters`
--

LOCK TABLES `posters` WRITE;
/*!40000 ALTER TABLE `posters` DISABLE KEYS */;
INSERT INTO `posters` VALUES (1,'p-godzila.jpeg','image/jpeg','30c4381db357df230af9be27c59fb913'),(2,'haos','',''),(3,'palma','',''),(4,'student','','');
/*!40000 ALTER TABLE `posters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `user_text` text NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`),
  CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,19,1,3,'Не очень','2021-03-29 22:31:23');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Администратор',NULL),(2,'Модератор',NULL),(3,'Пользователь',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Драма'),(2,'Хоррор'),(3,'Триллер'),(4,'Экшн'),(5,'Приключение'),(6,'Фантастика');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) DEFAULT NULL,
  `password_hash` varchar(256) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user','Иванов','Иван',NULL,'65E84BE33532FB784C48129675F9EFF3A682B27168C0EA744B2CF58EE02337C5',3),(7,'admin','Ализаде','Ульяна','Витальевна','65E84BE33532FB784C48129675F9EFF3A682B27168C0EA744B2CF58EE02337C5',1),(8,'moder','Кружалов','Алексей','Сергеевич','65E84BE33532FB784C48129675F9EFF3A682B27168C0EA744B2CF58EE02337C5',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-30  4:26:49
