# -*- coding: utf-8 -*-
"""
Задание 6.2a

Сделать копию скрипта задания 6.2.

Добавить проверку введенного IP-адреса. Адрес считается корректно заданным, если он:
   - состоит из 4 чисел (а не букв или других символов)
   - числа разделенны точкой
   - каждое число в диапазоне от 0 до 255

Если адрес задан неправильно, выводить сообщение:
'Неправильный IP-адрес'

Сообщение должно выводиться только один раз.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""
ip_adress= input('Введите IP-адрес в формате 10.0.1.1: ')

if ip_adress.count('.') == 3:
   ip_adress_list= ip_adress.split('.')
   for digit in ip_adress_list:
      try:
        int(digit)
      except ValueError:
        print('Неправильный IP-адрес')
        break
   else:
      if 1 <= int(ip_adress_list[0]) and int(ip_adress_list[0]) <= 223:
         print('unicast')
      elif 224 <= int(ip_adress_list[0]) and int(ip_adress_list[0]) <= 239:
         print('multicast')
      elif ip_adress=="255.255.255.255":
         print('local broadcast')
      elif ip_adress=="0.0.0.0":
         print('unassigned')
      else:
         print('unused')
else:
    print('Неправильный IP-адрес')
