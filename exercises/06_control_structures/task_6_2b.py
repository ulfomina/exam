# -*- coding: utf-8 -*-
"""
Задание 6.2b

Сделать копию скрипта задания 6.2a.

Дополнить скрипт:
Если адрес был введен неправильно, запросить адрес снова.

Ограничение: Все задания надо выполнять используя только пройденные темы.
"""
ip_adress= input('Введите IP-адрес в формате 10.0.1.1: ')
checking=False
while checking==False:
    if ip_adress.count('.') == 3:
        ip_adress_list= ip_adress.split('.')
        for digit in ip_adress_list:
             try:
                int(digit)
             except ValueError:
                print('Неправильный IP-адрес')
                
        else:
            if 1 <= int(ip_adress_list[0]) and int(ip_adress_list[0]) <= 223:
                print('unicast')
            elif 224 <= int(ip_adress_list[0]) and int(ip_adress_list[0]) <= 239:
                print('multicast')
            elif ip_adress=="255.255.255.255":
                 print('local broadcast')
            elif ip_adress=="0.0.0.0":
                print('unassigned')
            else:
                print('unused')
        checking=True
    else:
        print('Неправильный IP-адрес')
    if checking != True :
        ip_adress= input('Введите IP-адрес еще раз в формате 10.0.1.1: ')
