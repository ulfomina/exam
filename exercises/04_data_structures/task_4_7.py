# -*- coding: utf-8 -*-
"""
Задание 4.7

Преобразовать MAC-адрес в строке mac в двоичную строку такого вида:
'101010101010101010111011101110111100110011001100'

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

mac = "AAAA:BBBB:CCCC"
mac = mac.replace(':', '')
mac_list=list(mac)
print(mac_list)
mac_int= int(mac, 16)
mac_bin= bin(mac_int)
print(mac_bin)