# -*- coding: utf-8 -*-
"""
Задание 5.2

Запросить у пользователя ввод IP-сети в формате: 10.1.1.0/24

Затем вывести информацию о сети и маске в таком формате:

Network:
10        1         1         0
00001010  00000001  00000001  00000000

Mask:
/24
255       255       255       0
11111111  11111111  11111111  00000000

Проверить работу скрипта на разных комбинациях сеть/маска.

Подсказка: Получить маску в двоичном формате можно так:
In [1]: "1" * 28 + "0" * 4
Out[1]: '11111111111111111111111111110000'


Ограничение: Все задания надо выполнять используя только пройденные темы.
"""
network= input('Введите ip-сети в формате 0.0.0.0/00 ')
network= network.replace('.',' ')
network1= network.replace('/',' ')
network1= network1.split()

mask='1' * int(network1[-1]) + '0'*(32-int(network1[-1]))

print('Network:')
print('{:<8} {:<8} {:<8} {:<8}'.format(int(network1[0]), int(network1[1]),int(network1[2]), int(network1[3])))
print('{:08b} {:08b} {:08b} {:08b}'.format(int(network1[0]), int(network1[1]),int(network1[2]), int(network1[3])))
print('Mask:')
print('{:<8} {:<8} {:<8} {:<8}'.format(int(mask[0:8],2),int(mask[8:16],2),int(mask[16:24],2),int(mask[24:32],2)) ) 

print('{:08b} {:08b} {:08b} {:08b}'.format(int(mask[0:8],2),int(mask[8:16],2),int(mask[16:24],2),int(mask[24:32],2)) ) 
