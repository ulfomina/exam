# -*- coding: utf-8 -*-
"""
Задание 5.2b

Преобразовать скрипт из задания 5.2a таким образом,
чтобы сеть/маска не запрашивались у пользователя,
а передавались как аргумент скрипту.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""
from sys import argv

network= argv[1]
network= network.replace('.',' ')
network1= network.replace('/',' ')
network1= network1.split()

mask='1' * int(network1[-1]) + '0'*(32-int(network1[-1]))
print(mask)


print('Network:')

net2 = '{:08b}{:08b}{:08b}{:08b}'.format(int(network1[0]), int(network1[1]),int(network1[2]), int(network1[3]))
net2=net2[:-(32-int(network1[-1]))]+'0'*(32-int(network1[-1]))

print('{:8} {:8} {:8} {:8}'.format(int(net2[0:8],2), int(net2[8:16],2),int(net2[16:24],2), int(net2[24:32],2)))
print('{:8} {:8} {:8} {:8}'.format(net2[0:8], net2[8:16],net2[16:24], net2[24:32]))

print('Mask:')
print('{:8} {:8} {:8} {:8}'.format(int(mask[0:8],2),int(mask[8:16],2),int(mask[16:24],2),int(mask[24:32],2)) ) 

print('{:8} {:8} {:8} {:08}'.format(int(mask[0:8]),int(mask[8:16]),int(mask[16:24]),int(mask[24:32])) ) 
