# -*- coding: utf-8 -*-
"""
Задание 7.2b

Дополнить скрипт из задания 7.2a:
* вместо вывода на стандартный поток вывода,
  скрипт должен записать полученные строки в файл config_sw1_cleared.txt

При этом, должны быть отфильтрованы строки, которые содержатся в списке ignore.
Строки, которые начинаются на '!' отфильтровывать не нужно.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

ignore = ["duplex", "alias", "Current configuration"]
from sys import argv
file_text = argv[1]
with open(file_text, 'r')  as file, open('config_sw1_cleared.txt', 'a') as output:
  for line in file:

      ignore_line= False 
      for word in ignore:
         if line.find(word) > -1:
            ignore_line = True
            break
      if not ignore_line:
         output.write(line)
