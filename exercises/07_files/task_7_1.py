# -*- coding: utf-8 -*-
"""
Задание 7.1

Обработать строки из файла ospf.txt и вывести информацию по каждой строке в таком виде:

Prefix                10.0.24.0/24
AD/Metric             110/41
Next-Hop              10.0.13.3
Last update           3d18h
Outbound Interface    FastEthernet0/0

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""
with open('ospf.txt', 'r') as file:
    for line in file:
        line= line.replace(',','')
        file_list=line.split(' ')
        del file_list[0:8]
        
        prefix = file_list[0]
        AD = file_list[1].strip(' []')
        n_hop = file_list[3]
        update = file_list[4]
        interface = file_list[5].strip('\n')
        print('Prefix \t\t\t', prefix)
        print('AD/Metric \t\t', AD)
        print('Next-Hop \t\t', n_hop)
        print('Last update \t\t', update)
        print('Outbound Interface \t', interface)
