# -*- coding: utf-8 -*-
"""
Задание 7.3b

Сделать копию скрипта задания 7.3a.

Переделать скрипт:
- Запросить у пользователя ввод номера VLAN.
- Выводить информацию только по указанному VLAN.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""
with open('CAM_table.txt', 'r') as file:
    number_vlan = input('Введите номер VLAN: ')
    for line in file:
        if line.count('.') is 2:
            a = line.strip('\n').split()
            b = a.pop(-2)
            
            if a[0] == number_vlan:
                print("{:<8}{:18}{}".format(a[0], a[1], a[2]))